
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Wed Dec 16 16:44:47 CST 2015
//----------------------------------------------------

package Analizador;

/** CUP generated class containing symbol constants. */
public class SimbolosCUP {
  /* terminals */
  public static final int rbool = 41;
  public static final int menos = 31;
  public static final int sobreescribir = 68;
  public static final int menor = 27;
  public static final int rstring = 39;
  public static final int preced_cl = 96;
  public static final int pto = 7;
  public static final int nulo = 64;
  public static final int preced_op = 95;
  public static final int decremento = 36;
  public static final int asociativ_cl = 98;
  public static final int tchar = 106;
  public static final int importar = 47;
  public static final int asociativ_op = 97;
  public static final int interruptor = 70;
  public static final int fin = 10;
  public static final int cllave = 16;
  public static final int calgo = 14;
  public static final int igual2 = 24;
  public static final int aumento = 35;
  public static final int igual1 = 5;
  public static final int intstring = 75;
  public static final int rint = 37;
  public static final int mas = 30;
  public static final int menorigual = 29;
  public static final int mientras = 52;
  public static final int produce = 9;
  public static final int multiplicacion = 32;
  public static final int imprimir = 43;
  public static final int rdouble = 38;
  public static final int privado = 60;
  public static final int sino = 50;
  public static final int nonterm_cl = 88;
  public static final int caso = 44;
  public static final int doblepor = 2;
  public static final int gram_cl = 82;
  public static final int dolar = 3;
  public static final int nonterm_op = 87;
  public static final int sinosi = 56;
  public static final int gram_op = 81;
  public static final int defi = 78;
  public static final int doublestring = 76;
  public static final int ir = 45;
  public static final int sim_cl = 104;
  public static final int mayorigual = 28;
  public static final int sim_op = 103;
  public static final int nuevo = 67;
  public static final int id = 109;
  public static final int rtrue = 107;
  public static final int regresar = 48;
  public static final int rchar = 40;
  public static final int cod_cl = 84;
  public static final int dobledolar = 4;
  public static final int rfalse = 108;
  public static final int dec_cl = 80;
  public static final int cod_op = 83;
  public static final int protegido = 61;
  public static final int dec_op = 79;
  public static final int rand = 22;
  public static final int aalgo = 13;
  public static final int vacio = 62;
  public static final int mayor = 26;
  public static final int rxor = 19;
  public static final int breakpr = 42;
  public static final int esto = 69;
  public static final int doubleint = 77;
  public static final int division = 33;
  public static final int EOF = 0;
  public static final int supera = 63;
  public static final int tstring = 105;
  public static final int repetir = 71;
  public static final int lista_cl = 92;
  public static final int falso = 65;
  public static final int apar = 11;
  public static final int asoc_cl = 100;
  public static final int numero = 110;
  public static final int lista_op = 91;
  public static final int publico = 59;
  public static final int potencia = 34;
  public static final int asoc_op = 99;
  public static final int cpar = 12;
  public static final int verdadero = 66;
  public static final int error = 1;
  public static final int inicio_cl = 102;
  public static final int nombre_cl = 94;
  public static final int term_cl = 86;
  public static final int diferente = 25;
  public static final int si = 49;
  public static final int inicio_op = 101;
  public static final int ror = 21;
  public static final int extension = 58;
  public static final int nombre_op = 93;
  public static final int term_op = 85;
  public static final int acor = 17;
  public static final int salidastring = 72;
  public static final int hacer = 53;
  public static final int tipo_cl = 90;
  public static final int pasarint = 73;
  public static final int allave = 15;
  public static final int ccor = 18;
  public static final int ciclo = 55;
  public static final int tipo_op = 89;
  public static final int para = 51;
  public static final int dosp = 8;
  public static final int rnot = 23;
  public static final int coma = 6;
  public static final int ro = 20;
  public static final int clase = 46;
  public static final int flotanding = 111;
  public static final int oppotencia = 57;
  public static final int pasardouble = 74;
  public static final int hasta = 54;
}

