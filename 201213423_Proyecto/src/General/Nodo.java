/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package General;

/**
 *
 * @author Mynor Masaya
 */
public class Nodo {
    
    String tipo;
    int valor;
    String contenido;
    
    Nodo(String tipo, int valor, String contenido){
        this.tipo = tipo;
        this.valor = valor;
        this.contenido = contenido;
    }
    
    
    public void setTipo(String tipo){
        this.tipo = tipo;
    }
    
    public void setValor(int valor){
        this.valor = valor;
    }
    
    public void setContenido(String contenido){
        this.contenido = contenido;
    }
    
    public String getTipo(){
        return this.tipo;
    }
    
    public int getValor(){
        return this.valor;
    }
    
    public String getContenido(){
        return this.contenido;
    }
}
