package Analizador;
import java_cup.runtime.Symbol; 

%%

%{
//AQUI SE DECLARAN LAS LISTAS Y COSAS QUE VAMOS A USAR  
public String temp= "";
%}

%public
%class LexicoJFLEX
%cupsym SimbolosCUP
%cup
%char
%column
%full
%ignorecase
%line
%unicode

digito = [0-9]
numero = (" -")?{digito}+("." {digito}+)?
flotanding = (" -")?{digito}+("." {digito}+)
//numero = {digito}+("." {digito}+)?
tstring = "\"" ~"\""
tchar = "'" ~"'"
letra = [a-zA-ZñÑ]
id = {letra}+({letra}|{digito}|"_")*

%state COMENT_SIMPLE
%state COMENT_MULTI

%%

/*****************************************/
/*******        Comentarios        *******/
/*****************************************/
<YYINITIAL> "/*"               {yybegin(COMENT_MULTI);  temp="";}
<COMENT_MULTI> "*/"            {yybegin(YYINITIAL); System.out.println("Reconocido: <<"+temp+">>, Comentario Multi");}
<COMENT_MULTI> .|[ \t\r\n\f]    {temp+=yytext();}

<YYINITIAL> "//"           {yybegin(COMENT_SIMPLE); temp="";}
<COMENT_SIMPLE> .|[ \t\f]   {temp+=yytext();}
<COMENT_SIMPLE> "\n"        {yybegin(YYINITIAL); System.out.println("Reconocido: <<"+temp+">>, Comentario Simple");}


/*****************************************/
/*******        SIMBOLOS       *******/
/*****************************************/

<YYINITIAL> "%%"        {   System.out.println("Reconocido: <<"+yytext()+">>, doblepor");
                            return new Symbol(SimbolosCUP.doblepor, yycolumn, yyline, yytext());}

<YYINITIAL> "$"         {   System.out.println("Reconocido: <<"+yytext()+">>, dolar");
                            return new Symbol(SimbolosCUP.dolar, yycolumn, yyline, yytext());}

<YYINITIAL> "$$"        {   System.out.println("Reconocido: <<"+yytext()+">>, dobledolar");
                            return new Symbol(SimbolosCUP.dobledolar, yycolumn, yyline, yytext());}

<YYINITIAL> "="         {   System.out.println("Reconocido: <<"+yytext()+">>, igual1");
                            return new Symbol(SimbolosCUP.igual1, yycolumn, yyline, yytext());}

<YYINITIAL> ","         {   System.out.println("Reconocido: <<"+yytext()+">>, coma");
                            return new Symbol(SimbolosCUP.coma, yycolumn, yyline, yytext());}

<YYINITIAL> "."         {   System.out.println("Reconocido: <<"+yytext()+">>, pto");
                            return new Symbol(SimbolosCUP.pto, yycolumn, yyline, yytext());}

<YYINITIAL> ":"         {   System.out.println("Reconocido: <<"+yytext()+">>, dosp");
                            return new Symbol(SimbolosCUP.dosp, yycolumn, yyline, yytext());}

<YYINITIAL> "::="       {   System.out.println("Reconocido: <<"+yytext()+">>, produce");
                            return new Symbol(SimbolosCUP.produce, yycolumn, yyline, yytext());}

<YYINITIAL> ";"         {   System.out.println("Reconocido: <<"+yytext()+">>, fin");
                            return new Symbol(SimbolosCUP.fin, yycolumn, yyline, yytext());}

<YYINITIAL> "("         {   System.out.println("Reconocido: <<"+yytext()+">>, apar");
                            return new Symbol(SimbolosCUP.apar, yycolumn, yyline, yytext());}

<YYINITIAL> ")"         {   System.out.println("Reconocido: <<"+yytext()+">>, cpar");
                            return new Symbol(SimbolosCUP.cpar, yycolumn, yyline, yytext());}

<YYINITIAL> "{:"         {   System.out.println("Reconocido: <<"+yytext()+">>, aalgo");
                            return new Symbol(SimbolosCUP.aalgo, yycolumn, yyline, yytext());}

<YYINITIAL> ":}"         {   System.out.println("Reconocido: <<"+yytext()+">>, calgo");
                            return new Symbol(SimbolosCUP.calgo, yycolumn, yyline, yytext());}

<YYINITIAL> "{"         {   System.out.println("Reconocido: <<"+yytext()+">>, allave");
                            return new Symbol(SimbolosCUP.allave, yycolumn, yyline, yytext());}

<YYINITIAL> "}"         {   System.out.println("Reconocido: <<"+yytext()+">>, cllave");
                            return new Symbol(SimbolosCUP.cllave, yycolumn, yyline, yytext());}

<YYINITIAL> "["         {   System.out.println("Reconocido: <<"+yytext()+">>, acor");
                            return new Symbol(SimbolosCUP.acor, yycolumn, yyline, yytext());}

<YYINITIAL> "]"         {   System.out.println("Reconocido: <<"+yytext()+">>, ccor");
                            return new Symbol(SimbolosCUP.ccor, yycolumn, yyline, yytext());}
							
/*****************************************/
/*******        OPERADORES LOGICOS       *******/
/*****************************************/

<YYINITIAL> "??"        {   System.out.println("Reconocido: <<"+yytext()+">>, rxor");
                            return new Symbol(SimbolosCUP.rxor, yycolumn, yyline, yytext());}

<YYINITIAL> "|"         {   System.out.println("Reconocido: <<"+yytext()+">>, ro");
                            return new Symbol(SimbolosCUP.ro, yycolumn, yyline, yytext());}

<YYINITIAL> "||"        {   System.out.println("Reconocido: <<"+yytext()+">>, ror");
                            return new Symbol(SimbolosCUP.ror, yycolumn, yyline, yytext());}

<YYINITIAL> "&&"        {   System.out.println("Reconocido: <<"+yytext()+">>, rand");
                            return new Symbol(SimbolosCUP.rand, yycolumn, yyline, yytext());}

<YYINITIAL> "¡"        {   System.out.println("Reconocido: <<"+yytext()+">>, rnot");
                            return new Symbol(SimbolosCUP.rnot, yycolumn, yyline, yytext());}
							
/**/
/*RELACINALES*/
/**/
<YYINITIAL> "=="        {   System.out.println("Reconocido: <<"+yytext()+">>, igual2");
                            return new Symbol(SimbolosCUP.igual2, yycolumn, yyline, yytext());}

<YYINITIAL> "!="        {   System.out.println("Reconocido: <<"+yytext()+">>, diferente");
                            return new Symbol(SimbolosCUP.diferente, yycolumn, yyline, yytext());}

<YYINITIAL> ">"         {   System.out.println("Reconocido: <<"+yytext()+">>, mayor1");
                            return new Symbol(SimbolosCUP.mayor, yycolumn, yyline, yytext());}

<YYINITIAL> "<"         {   System.out.println("Reconocido: <<"+yytext()+">>, menor1");
                            return new Symbol(SimbolosCUP.menor, yycolumn, yyline, yytext());}

<YYINITIAL> ">="   {   System.out.println("Reconocido: <<"+yytext()+">>, mayorigual");
                            return new Symbol(SimbolosCUP.mayorigual, yycolumn, yyline, yytext());}

<YYINITIAL> "<="  {   System.out.println("Reconocido: <<"+yytext()+">>, menorigual");
                            return new Symbol(SimbolosCUP.menorigual, yycolumn, yyline, yytext());}	
							
/**/
/*ARITMETICOS*/
/**/
<YYINITIAL> "+"         {   System.out.println("Reconocido: <<"+yytext()+">>, mas");
                            return new Symbol(SimbolosCUP.mas, yycolumn, yyline, yytext());}

<YYINITIAL> "-"         {   System.out.println("Reconocido: <<"+yytext()+">>, menos");
                            return new Symbol(SimbolosCUP.menos, yycolumn, yyline, yytext());}

<YYINITIAL> "*"         {   System.out.println("Reconocido: <<"+yytext()+">>, multiplicacion");
                            return new Symbol(SimbolosCUP.multiplicacion, yycolumn, yyline, yytext());}

<YYINITIAL> "/"         {   System.out.println("Reconocido: <<"+yytext()+">>, divicion");
                            return new Symbol(SimbolosCUP.division, yycolumn, yyline, yytext());}

<YYINITIAL> "^"         {   System.out.println("Reconocido: <<"+yytext()+">>, potencia");
                            return new Symbol(SimbolosCUP.potencia, yycolumn, yyline, yytext());}

<YYINITIAL> "++"        {   System.out.println("Reconocido: <<"+yytext()+">>, aumento");
                            return new Symbol(SimbolosCUP.aumento, yycolumn, yyline, yytext());}

<YYINITIAL> "--"        {   System.out.println("Reconocido: <<"+yytext()+">>, decremento");
                            return new Symbol(SimbolosCUP.decremento, yycolumn, yyline, yytext());}							


/*****************************************/
/*******     SECCIONES    *******/
/*****************************************/

<YYINITIAL> "<Dec>" 	{   System.out.println("Reconocido: <<"+yytext()+">>, dec_op ");
                                return new Symbol(SimbolosCUP.dec_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</Dec>"  {   System.out.println("Reconocido: <<"+yytext()+">>, dec_cl ");
                                return new Symbol(SimbolosCUP.dec_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<Gram>"  {   System.out.println("Reconocido: <<"+yytext()+">>, gram_op ");
                                return new Symbol(SimbolosCUP.gram_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</Gram>" {   System.out.println("Reconocido: <<"+yytext()+">>, gram_cl ");
                                return new Symbol(SimbolosCUP.gram_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<Cod>" {   System.out.println("Reconocido: <<"+yytext()+">>, cod_op ");
                                return new Symbol(SimbolosCUP.cod_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</Cod>"  {   System.out.println("Reconocido: <<"+yytext()+">>, cod_cl ");
                                return new Symbol(SimbolosCUP.cod_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<Terminal>"  {   System.out.println("Reconocido: <<"+yytext()+">>, term_op ");
                                return new Symbol(SimbolosCUP.term_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</Terminal>"  {   System.out.println("Reconocido: <<"+yytext()+">>, term_cl ");
                                return new Symbol(SimbolosCUP.term_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<nonTerminal>"  {   System.out.println("Reconocido: <<"+yytext()+">>, nonterm_op ");
                                return new Symbol(SimbolosCUP.nonterm_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</nonTerminal>"  {   System.out.println("Reconocido: <<"+yytext()+">>, nonterm_cl ");
                                return new Symbol(SimbolosCUP.nonterm_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<tipo>" {   System.out.println("Reconocido: <<"+yytext()+">>, tipo_op ");
                                return new Symbol(SimbolosCUP.tipo_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</tipo>"  {   System.out.println("Reconocido: <<"+yytext()+">>, tipo_cl ");
                                return new Symbol(SimbolosCUP.tipo_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<Lista>"  {   System.out.println("Reconocido: <<"+yytext()+">>, lista_op ");
                                return new Symbol(SimbolosCUP.lista_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</Lista>"  {   System.out.println("Reconocido: <<"+yytext()+">>, lista_cl ");
                                return new Symbol(SimbolosCUP.lista_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<nombre>"  {   System.out.println("Reconocido: <<"+yytext()+">>, nombre_op ");
                                return new Symbol(SimbolosCUP.nombre_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</nombre>"  {   System.out.println("Reconocido: <<"+yytext()+">>, nombre_cl ");
                                return new Symbol(SimbolosCUP.nombre_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<precedencia>"  {   System.out.println("Reconocido: <<"+yytext()+">>, preced_op ");
                                return new Symbol(SimbolosCUP.preced_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</precedencia>"  {   System.out.println("Reconocido: <<"+yytext()+">>, preced_cl ");
                                return new Symbol(SimbolosCUP.preced_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<asociatividad>"  {   System.out.println("Reconocido: <<"+yytext()+">>, asociativ_op ");
                                return new Symbol(SimbolosCUP.asociativ_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</asociatividad>"  {   System.out.println("Reconocido: <<"+yytext()+">>, asociativ_cl ");
                                return new Symbol(SimbolosCUP.asociativ_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<asociacion>"  {   System.out.println("Reconocido: <<"+yytext()+">>, asoc_op ");
                                return new Symbol(SimbolosCUP.asoc_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</asociacion>"  {   System.out.println("Reconocido: <<"+yytext()+">>, asoc_cl ");
                                return new Symbol(SimbolosCUP.asoc_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<inicio>"  {   System.out.println("Reconocido: <<"+yytext()+">>, inicio_op ");
                                return new Symbol(SimbolosCUP.inicio_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</inicio>"  {   System.out.println("Reconocido: <<"+yytext()+">>, inicio_cl ");
                                return new Symbol(SimbolosCUP.inicio_cl, yycolumn, yyline, yytext());}


<YYINITIAL> "<sim>"  {   System.out.println("Reconocido: <<"+yytext()+">>, sim_op ");
                                return new Symbol(SimbolosCUP.sim_op, yycolumn, yyline, yytext());}


<YYINITIAL> "</sim>" {   System.out.println("Reconocido: <<"+yytext()+">>, sim_cl ");
                                return new Symbol(SimbolosCUP.sim_cl, yycolumn, yyline, yytext());}

/*****************************************/
/******* reservadas tipos de datos *******/
/*****************************************/
<YYINITIAL> "int"       {   System.out.println("Reconocido: <<"+yytext()+">>, rint");
                            return new Symbol(SimbolosCUP.rint, yycolumn, yyline, yytext());}

<YYINITIAL> "double"    {   System.out.println("Reconocido: <<"+yytext()+">>, rdouble");
                                    return new Symbol(SimbolosCUP.rdouble, yycolumn, yyline, yytext());}

<YYINITIAL> "string"    {   System.out.println("Reconocido: <<"+yytext()+">>, rstring");
                            return new Symbol(SimbolosCUP.rstring, yycolumn, yyline, yytext());}

<YYINITIAL> "char"      {   System.out.println("Reconocido: <<"+yytext()+">>, rchar");
                            return new Symbol(SimbolosCUP.rchar, yycolumn, yyline, yytext());}

<YYINITIAL> "booleano"      {   System.out.println("Reconocido: <<"+yytext()+">>, rbool");
                            return new Symbol(SimbolosCUP.rbool, yycolumn, yyline, yytext());}
							
							
/*****************************************/
/*******     reservadas sistema     *******/
/*****************************************/



<YYINITIAL> "break" {System.out.println("break"); return new Symbol(SimbolosCUP.breakpr, yytext());}

<YYINITIAL> "default" {System.out.println("default"); return new Symbol(SimbolosCUP.defi, yytext());}

<YYINITIAL> "print" {System.out.println("print"); return new Symbol(SimbolosCUP.imprimir, yytext());}

<YYINITIAL> "case" {System.out.println("case"); return new Symbol(SimbolosCUP.caso, yytext());}

<YYINITIAL> "go" {System.out.println("ir"); return new Symbol(SimbolosCUP.ir, yytext());}

<YYINITIAL> "class" {System.out.println("class"); return new Symbol(SimbolosCUP.clase, yytext());}

<YYINITIAL> "import" {System.out.println("import"); return new Symbol(SimbolosCUP.importar, yytext());}

<YYINITIAL> "return" {System.out.println("return"); return new Symbol(SimbolosCUP.regresar, yytext());}

<YYINITIAL> "if" {System.out.println("if"); return new Symbol(SimbolosCUP.si, yytext());}

<YYINITIAL> "else" {System.out.println("else"); return new Symbol(SimbolosCUP.sino, yytext());}

<YYINITIAL> "for" {System.out.println("for"); return new Symbol(SimbolosCUP.para, yytext());}

<YYINITIAL> "while" {System.out.println("while"); return new Symbol(SimbolosCUP.mientras, yytext());}

<YYINITIAL> "do" {System.out.println("do"); return new Symbol(SimbolosCUP.hacer, yytext());}

<YYINITIAL> "until" {System.out.println("until"); return new Symbol(SimbolosCUP.hasta, yytext());}

<YYINITIAL> "loop" {System.out.println("loop"); return new Symbol(SimbolosCUP.ciclo, yytext());}

<YYINITIAL> "elseif" {System.out.println("elseif"); return new Symbol(SimbolosCUP.sinosi, yytext());}

<YYINITIAL> "pow" {System.out.println("pow"); return new Symbol(SimbolosCUP.oppotencia, yytext());}

<YYINITIAL> "extends" {System.out.println("extends"); return new Symbol(SimbolosCUP.extension, yytext());}

<YYINITIAL> "public" {System.out.println("public"); return new Symbol(SimbolosCUP.publico, yytext());}

<YYINITIAL> "private" {System.out.println("private"); return new Symbol(SimbolosCUP.privado, yytext());}

<YYINITIAL> "protected" {System.out.println("protected"); return new Symbol(SimbolosCUP.protegido, yytext());}

<YYINITIAL> "void" {System.out.println("void"); return new Symbol(SimbolosCUP.vacio, yytext());}

<YYINITIAL> "super" {System.out.println("super"); return new Symbol(SimbolosCUP.supera, yytext());}

<YYINITIAL> "null" {System.out.println("null"); return new Symbol(SimbolosCUP.nulo, yytext());}



<YYINITIAL> "new" {return new Symbol(SimbolosCUP.nuevo, yytext()); } 

<YYINITIAL> "<!Override!>" {return new Symbol(SimbolosCUP.sobreescribir, yytext()); }

<YYINITIAL> "THIS" 			{return new Symbol(SimbolosCUP.esto, yytext()); }  

<YYINITIAL> "switch" 		{return new Symbol(SimbolosCUP.interruptor, yytext()); } 

<YYINITIAL> "repetir" 	{return new Symbol(SimbolosCUP.repetir, yytext()); } 

<YYINITIAL> "out_string" {return new Symbol(SimbolosCUP.salidastring, yytext()); }

<YYINITIAL> "ParseInt" {return new Symbol(SimbolosCUP.pasarint, yytext()); }

<YYINITIAL> "ParseDouble" {return new Symbol(SimbolosCUP.pasardouble, yytext()); }

<YYINITIAL> "intToStr" {return new Symbol(SimbolosCUP.intstring, yytext()); }  

<YYINITIAL> "doubleToStr" {return new Symbol(SimbolosCUP.doublestring, yytext()); }   

<YYINITIAL> "doubleToInt" {return new Symbol(SimbolosCUP.doubleint, yytext()); } 		



/*****************************************/
/*******     PARTE TOKEN     *******/
/*****************************************/


<YYINITIAL> {numero}        {   System.out.println("Reconocido: <<"+yytext()+">>, numero ");
                                return new Symbol(SimbolosCUP.numero, yycolumn, yyline, yytext());}
								
<YYINITIAL> {flotanding}        {   System.out.println("Reconocido: <<"+yytext()+">>, float ");
                                return new Symbol(SimbolosCUP.flotanding, yycolumn, yyline, yytext());}

<YYINITIAL> {tstring}       {   System.out.println("Reconocido: <<"+yytext()+">>, tstring ");
                                return new Symbol(SimbolosCUP.tstring, yycolumn, yyline, yytext());}

<YYINITIAL> {tchar}         {   System.out.println("Reconocido: <<"+yytext()+">>, tchar ");
                                return new Symbol(SimbolosCUP.tchar, yycolumn, yyline, yytext());}

<YYINITIAL> {id}            {   System.out.println("Reconocido: <<"+yytext()+">>, id ");
                                return new Symbol(SimbolosCUP.id, yycolumn, yyline, yytext()); }

<YYINITIAL> "false"         {   System.out.println("Reconocido: <<"+yytext()+">>, rfalse");
                                return new Symbol(SimbolosCUP.rfalse, yycolumn, yyline, yytext());}

<YYINITIAL> "true"          {   System.out.println("Reconocido: <<"+yytext()+">>, rtrue");
                                return new Symbol(SimbolosCUP.rtrue, yycolumn, yyline, yytext());}								



/*****************************************/
/*******     PARTE TOKEN     *******/
/*****************************************/


								
                            

[ \t\r\n\f]                 {/* ignore white space. */ }
 
.                           {   System.out.println("Error Lexico: <<"+yytext()+">> ["+yyline+" , "+yycolumn+"]"); }

